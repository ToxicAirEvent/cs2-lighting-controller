# CS:GO/CS2 Lighting Controller

## About:

This script utilizes the [Global Offensive Game State Integration](https://developer.valvesoftware.com/wiki/Counter-Strike:_Global_Offensive_Game_State_Integration) to manage lighting or any other action triggered via a webhook.
It determines the lighting based on the side you're currently playing or if you're idle in the game.

## Technical Notes:
The workflow for the system is as follows:

* Set up a Game State Integration broadcaster for this application in your CS:GO or CS2 installation.
* The Game State Integration file will prompt the server where this application is running. Specifically, it will attempt to access the `/game-data` endpoint.
* The light controller will process the current game state received from CS:GO/CS2 through a `POST request`.
* It will trigger the webhook specified in the `.env` file corresponding to the current game state.
* Your endpoint service with the webhooks will execute the programmed actions.

## Installation:
* Clone this repository to your server or desired location for running the lighting webhooks server.
* Ensure you have Node.js and NPM installed on your machine. Run `npm install` to install all the required packages for the application.
* Open port `8003` on your machine to allow CS2/CS:GO to access it.
* Configure the `.env-example` file to include the desired webhooks for each game state. Save it as `.env`.
* Boot the script with `nodemon index.js`. Alternatively, you can start it with `node ./index.js`, but Nodemon is better at recovering from errors and automatically rebooting if any files are changed or updated.
* Adjust the example Game State Integration file by replacing `<YOUR_SERVER_IP_HERE>` with your server's IP. Save it to the cfg folder for CS:GO/CS2. Your saved file must start with `gamestate_integration_` but can be postfixed with whatever you wish.^
* Enter a game and allow up to 90 seconds to see if your webhooks trigger. If not, troubleshoot accordingly.

^Note that there are multiple `cfg` folders in the CS2 installation. The script typically works from the `\Counter-Strike Global Offensive\game\csgo\cfg` folder, but you may need to experiment.

## License

For commercial use, please contact me to discuss licensing terms. If you're using it for personal enjoyment while playing, feel free to use or modify it as needed. I provide no warranty and cannot guarantee support.

You assume responsibility for any issues arising from the use of this script.
