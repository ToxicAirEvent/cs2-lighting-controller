require('dotenv').config()
const axios = require('axios');
const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');

const app = express(express.json())

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());

app.get('/', function (req, res) {
  res.send('Light Controller Running.')
})

app.post('/game-data', async function(req, res){
	const gameData = req.body;

	const transmittingPlayer = gameData.provider.steamid
	const currentAcitivity = gameData?.player?.activity || 'idle' //If you can't get a state of the player just assume they are idle.
	const currentSide = gameData?.player?.team || null

	console.log("GSI Data:")
	console.log(`Transmitting Player Steam ID: ${transmittingPlayer}`)
	console.log(`Player Info:`)
	console.log(gameData.player)
	console.log(`Resolved Player Acivity: ${currentAcitivity}`)
	console.log(`Resolved Current Side: ${currentSide}`)

	console.log("All Game Data:")
	console.log(gameData)

	if(currentAcitivity === 'playing'){
		if(currentSide === 'T'){
			try{
				await axios.get(process.env.T_SIDE_WEBHOOK)
				return res.status(200).send('Lights T');
			}catch(error){
				console.error("T light error.")
				console.error(error)
			}
		}
		
		if(currentSide === 'CT'){
			try{
				await axios.get(process.env.CT_SIDE_WEBHOOK)
				return res.status(200).send('Lights CT');
			}catch(error){
				console.error("CT light error.")
				console.error(error)
			}
		}
	}

	try{
		await axios.get(process.env.IDLE_WEBHOOK)
	}catch(error){
				console.error("Error setting idle lights.")
				console.error(error)
			}

	return res.status(200).send('Lights idle.');
})

app.listen(8003)